import cv2
import numpy as np
from glob import glob
from random import randrange
from image import crop_image

files = [None]

def nothing(x):
    pass

def get_image(i):
    if files[0] is None:
        files[0] = current_files = glob("assets/processed_pics/*") 
    else:
        current_files = files[0]
    current_files = sorted(current_files, key=lambda x: int(x.split('frame')[1].split('.jpg')[0]))

    img = cv2.imread(current_files[i])
    img = crop_image(img)
    return img

def open_picker():
    # Get image
    img = get_image(0)

    # Create a window
    cv2.namedWindow('image')
    
    # create trackbars for color change
    cv2.createTrackbar('lowH','image',0,180,nothing)
    cv2.createTrackbar('highH','image',180,180,nothing)
    
    cv2.createTrackbar('lowS','image',0,255,nothing)
    cv2.createTrackbar('highS','image',255,255,nothing)
    
    cv2.createTrackbar('lowV','image',0,255,nothing)
    cv2.createTrackbar('highV','image',255,255,nothing)

    cv2.createTrackbar('Image','image',0,len(files[0])-1, nothing)
    
    while(True):
        num = cv2.getTrackbarPos('Image', 'image')
        img = get_image(num)

        frame = img 
        # get current positions of the trackbars
        ilowH = cv2.getTrackbarPos('lowH', 'image')
        ihighH = cv2.getTrackbarPos('highH', 'image')
        ilowS = cv2.getTrackbarPos('lowS', 'image')
        ihighS = cv2.getTrackbarPos('highS', 'image')
        ilowV = cv2.getTrackbarPos('lowV', 'image')
        ihighV = cv2.getTrackbarPos('highV', 'image')
        
        # convert color to hsv because it is easy to track colors in this color model
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        lower_hsv = np.array([ilowH, ilowS, ilowV])
        higher_hsv = np.array([ihighH, ihighS, ihighV])
        # Apply the cv2.inrange method to create a mask
        mask = cv2.inRange(hsv, lower_hsv, higher_hsv)
        # Apply the mask on the image to extract the original color
        frame = cv2.bitwise_and(frame, frame, mask=mask)
        cv2.imshow('image', frame)
        # Press q to exit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    cv2.destroyAllWindows()